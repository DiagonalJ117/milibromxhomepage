const currentYear = new Date().getFullYear();

const months = {
    jan: new Date(currentYear, 0),
    feb: new Date(currentYear, 1),
    mar: new Date(currentYear, 2),
    apr: new Date(currentYear, 3),
    may: new Date(currentYear, 4),
    jun: new Date(currentYear, 5),
    jul: new Date(currentYear, 6),
    aug: new Date(currentYear, 7),
    sep: new Date(currentYear, 8),
    oct: new Date(currentYear, 9),
    nov: new Date(currentYear, 10),
    dec: new Date(currentYear, 11),
}

export {
    months,
    currentYear
}