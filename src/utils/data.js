import React from "react";
import LocalShippingIcon from '@material-ui/icons/LocalShipping';
import CardGiftcardIcon from '@material-ui/icons/CardGiftcard';
import LocalLibraryIcon from '@material-ui/icons/LocalLibrary';
import clubninosimg from '../assets/clubdelecturaninios.jpg';
import clubadultosimg from '../assets/clubdelecturaadultos.jpg';
import clubteensimg from '../assets/clubdelectura.jpg';
import BookmarksIcon from '@material-ui/icons/Bookmarks';
import CollectionsBookmarkIcon from '@material-ui/icons/CollectionsBookmark';
import isjuventud from '../assets/isjuventud.jpg'
import finalistapge from '../assets/finalistapge.jpg'
import presentacionlibro from '../assets/presentacionlibro.jpg'
const iconSize = 30;
const servicios = [
    {
        color: "#00C853",
        headline: "Servicio a domicilio tipo florería",
        text:
          "Somos de Hermosillo, Sonora y hacemos envíos a domicilio a todo México. Contamos con servicio personalizado tipo florería, es decir se lo entregamos a quien desees regalar un libro.",
        icon: <LocalShippingIcon style={{ fontSize: iconSize }} />,
        mdDelay: "0",
        smDelay: "0",
        details: "$40* Hermosillo / $70 a $90 Ciudades de Sonora"
      },
      {
        color: "#6200EA",
        headline: "Envoltura para regalo",
        text:
          "Nuestras envolturas para regalo están hechas con materiales reciclables. Deseamos que quien reciba un libro envuelto tenga una experiencia agradable e inolvidable y se motive a leer. No regales flores, ¡Regala un libro!",
        icon: <CardGiftcardIcon style={{ fontSize: iconSize }} />,
        mdDelay: "200",
        smDelay: "200"
      },
      {
        color: "#0091EA",
        headline: "Clubs de Lectura",
        text:
          "Desde el año 2018 contamos con 3 Clubs de lectura en Hermosillo, Sonora",
        icon: <LocalLibraryIcon style={{ fontSize: iconSize }} />,
        mdDelay: "400",
        smDelay: "0",
      },
]

const clubs = {
    clubNinios: {
        title: "Club para Niños",
        subtitle: "(De 6 a 11 años de edad)",
        moderators: {
            mod1: {
                name: "Grecia Corrales",
                title: "Moderadora"
            }
        },
        tileData: {
            img: clubninosimg,
            title: 'Club de lectura para niñas y niños lectores',
            subtitle: 'Dirigido por Mariana Paz de milibromx y la profesora Grecia Margoth Corrales',
            featured: true
        },
        time: {
            day: "Sábado",
            startTime: "16:00",
            endTime: "18:00",
            label: "Horario"
        }
    },
    clubTeens: {
        title: "Club para Adolescentes",
        subtitle: "(De 12 a 21 años de edad)",
        moderators: {
            mod1: {
                name: "Mariana Paz",
                title: "Moderadora"
            }
        },
        tileData: {
            img: clubteensimg,
            title: 'Club de lectura para adolescentes lectores',
            subtitle: 'Dirigido por Mariana Paz de milibromx, Booktuber Lizeth López y la escritora e iustradora Esther Gracida.',
            featured: true
        },
        time: {
            day: "Martes",
            startTime: "16:00",
            endTime: "18:00",
            label: "Horario"
        },
        calendar:{
            enabled: true,
            title: `Calendario Club de Lectura Adolescentes ${new Date().getFullYear()}`
        } 
    },
    clubAdultos: {
        title: "Club para Jóvenes / Adultos",
        subtitle: "(A partir de 18 años de edad)",
        moderators: {
            mod1: {
                name: "Lizeth López",
                title: "Moderadora"
            }
        },
        tileData: {
            img: clubadultosimg,
            title: 'Club de jóvenes adultos',
            subtitle: 'Dirigido por la booktuber Lizeth López',
            featured: true
        },
        time: {
            day: "Una sesión al mes.",
            startTime: "16:00",
            endTime: "18:00",
            label: "Horario"
        }
    }
}

const creaciones = [
    {
        icon: <BookmarksIcon/>,
        text: "Accesorios para leer (Separadores de página, lámparas de lectura, bolsas de viaje) 🔖🛍"
    },
    {
        icon: <CollectionsBookmarkIcon/>,
        text: "Venta de libros nuevos📚"
    },
    {
        icon: <LocalShippingIcon />,
        text: "Envíos a todo México📭"
    },
    {
        icon: <LocalLibraryIcon />,
        text: "Clubs de lectura gratuitos para niños, adolescentes y adultos."
    },
]

const photoList = [
    {
        img: clubteensimg,
        title: 'Club de lectura para adolescentes lectores',
        subtitle: 'Dirigido por Mariana Paz de milibromx, Booktuber Lizeth López y la escritora e iustradora Esther Gracida.',
        featured: true
    },
    {
        img: clubninosimg,
        title: 'Club de lectura para niñas y niños lectores',
        subtitle: 'Dirigido por Mariana Paz de milibromx y la profesora Grecia Margoth Corrales',
        featured: true
    },
    {
        img: clubadultosimg,
        title: 'Club de jóvenes adultos',
        subtitle: 'Dirigido por la booktuber Lizeth López',
        featured: true
    },
    {
        img: finalistapge,
        title: 'Cuento Finalista de Mariana Paz: "Nunca Hagas Enojar a tu Amigo Imaginario"',
        subtitle: 'El cual se encuentra en el libro "Antología Ilustrada 2", presentado en la Feria Internacional del Libro 2018 en Guadalajara, Jalisco',
        featured: true
    },
    {
        img: isjuventud,
        title: 'Mariana Paz de milibromx Finalista del Premio Estatal de la Juventud 2018',
        subtitle: 'En la categoría Ingenio Emprendedor',
        featured: true
    },
    {
        img: presentacionlibro,
        title: 'Mariana Paz presentando el libro del genero Novela negra, "Yo soy el araña" del escritor Carlos René Padilla.',
        featured: true
    },
]

export {
    servicios,
    clubs,
    creaciones,
    photoList
}