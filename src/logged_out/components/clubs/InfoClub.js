import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import { Box, Grid, Typography, GridList, GridListTile, GridListTileBar, List, ListItemAvatar, Avatar, ListItemText, ListItem } from "@material-ui/core";
import StarIcon from '@material-ui/icons/Star';
import ScheduleIcon from '@material-ui/icons/Schedule';
import 'react-day-picker/lib/style.css';
import ClubCalendar from './ClubCalendar';

const useStyles = makeStyles((theme) => ({
    root: {
      display: 'flex',
      flexWrap: 'wrap',
      justifyContent: 'space-around',
      overflow: 'hidden',
      backgroundColor: theme.palette.background.paper,
    },
    gridList: {
      // Promote the list into its own layer on Chrome. This cost memory but helps keeping high FPS.
      transform: 'translateZ(0)',
    },
    titleBar: {
      textAlign:"center",
      background: 'rgba(0,0,0,0.8)'
    },
    icon: {
      color: 'white',
    },
    container: {
        width: '100%',
        alignSelf: 'center',
        alignItems: 'stretch'
    }
  }));

const InfoClub = (props) => {
    const classes = useStyles();
    const { club } = props;
    const { title, subtitle, tileData, moderators, time, calendar } = club;
    return (
        <Box justifyContent="center"  >
        <Typography variant="h3" align="center">
            {title}
        </Typography>
        <Typography variant="h5" align="center">
            {subtitle}
        </Typography>
        <Grid container direction="column" >
            <Grid item>
                <GridList className={classes.gridList} style={{marginTop:"30px"}}>
              
                <GridListTile key={tileData.img} cols={tileData.featured ? 2 : 1} rows={tileData.featured ? 2 : 1}>
                    <img src={tileData.img} alt={tileData.title} />
                    <GridListTileBar
                    title={tileData.title}
                    subtitle={tileData.subtitle}
                    titlePosition="bottom"
                    className={classes.titleBar}
                    />
                </GridListTile>
                
            </GridList>
            </Grid>
            <Grid item>
                <Grid container direction="row" justify="space-between">
                    <List>
                        <ListItem>
                            <ListItemAvatar>
                                <Avatar>
                                    <StarIcon/>
                                </Avatar>
                            </ListItemAvatar>
                            <ListItemText primary={moderators.mod1.name} secondary={moderators.mod1.title}/>
                        </ListItem>
                        <ListItem>
                            <ListItemAvatar>
                                <Avatar>
                                    <ScheduleIcon/>
                                </Avatar>
                            </ListItemAvatar>
                            <ListItemText primary={`${time.day} ${time.startTime} - ${time.endTime} Horas`} secondary={time.label}/>
                        </ListItem>
                    </List>
                </Grid>
            </Grid>
            {calendar && (
                <Grid container direction="row" justify="center" alignItems="stretch">
                    <ClubCalendar title={calendar.title} />
                </Grid>
            )}
        </Grid>
     </Box>
    )
}

export default InfoClub
