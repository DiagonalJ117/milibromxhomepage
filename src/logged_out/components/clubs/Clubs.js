import React, { Fragment } from "react";
import ClubsSec1 from "./ClubsSec1";
import ClubsSec2 from "./ClubsSec2";



function Clubs() {

  return (
    <Fragment>
      <ClubsSec1/>
      <ClubsSec2/>
    </Fragment>
  );
}



export default Clubs;