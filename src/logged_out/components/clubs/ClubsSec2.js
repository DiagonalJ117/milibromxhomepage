import React from "react";
import PropTypes from "prop-types";
import { Grid, Typography, withWidth, AppBar, Box } from "@material-ui/core";
import calculateSpacing from "../home/calculateSpacing";
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import SwipeableViews from 'react-swipeable-views';
import { useTheme } from '@material-ui/core/styles';
import {clubs} from '../../../utils/data'
import InfoClub from "./InfoClub";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `full-width-tab-${index}`,
    'aria-controls': `full-width-tabpanel-${index}`,
  };
}

function ClubsSec2(props) {
  const { width } = props;
  const theme = useTheme();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const handleChangeIndex = (index) => {
    setValue(index);
  };

  return (
    <div style={{ backgroundColor: "#FFFFFF" }}>
            <AppBar position="static" color="default">
              
        <Tabs
          value={value}
          onChange={handleChange}
          indicatorColor="primary"
          textColor="primary"
          variant="fullWidth"
          aria-label="full width tabs example"
        >
          <Tab label="Niños" {...a11yProps(0)} />
          <Tab label="Adolescentes" {...a11yProps(1)} />
          <Tab label="Jóvenes/Adultos" {...a11yProps(2)} />
        </Tabs>
      </AppBar>
      <div className="container-fluid lg-p-top">

        <div className="container-fluid">
      <Grid container spacing={calculateSpacing(width)} justify="center">
          <SwipeableViews
        axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
        index={value}
        onChangeIndex={handleChangeIndex}
      >
        <TabPanel value={value} index={0} dir={theme.direction}>
          <InfoClub club={clubs.clubNinios} />
        </TabPanel>
        <TabPanel value={value} index={1} dir={theme.direction}>
        <InfoClub club={clubs.clubTeens} />
        </TabPanel>
        <TabPanel value={value} index={2} dir={theme.direction}>
          <InfoClub club={clubs.clubAdultos} />
        </TabPanel>
      </SwipeableViews>
          </Grid>
        </div>
      </div>
    </div>
  );
}

ClubsSec2.propTypes = {
  width: PropTypes.string.isRequired
};

export default withWidth()(ClubsSec2);
