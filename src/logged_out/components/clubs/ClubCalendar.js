import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import { Card, CardContent } from "@material-ui/core";
import DayPicker from 'react-day-picker';
import 'react-day-picker/lib/style.css';
import CardHeader from "@material-ui/core/CardHeader";
import clubDates from '../../../utils/clubDates';
import { MONTHS, WEEKDAYS, WEEKDAYS_SHORT } from '../../../utils/calendarText';
import { months } from '../../../utils/currentYear';

const useStyles = makeStyles((theme) => ({
    wrapper: {
        backgroundColor: theme.palette.primary,
        textAlign: 'center'
    },
    cardTitle: {
        textAlign: 'center',
        backgroundColor: theme.palette.secondary.main,
        color: theme.palette.common.white
    }
}));


const CalendarWrapper = (props) => {
    const classes = useStyles();
    const { children, title } = props;

    return(
        <Card className={classes.wrapper}>
            <CardHeader title={title} className={classes.cardTitle} />
            <CardContent>
                {children}
            </CardContent>
        </Card>

    )
}

const ClubCalendar = (props) => {
    const { title } = props;
    
    return(
        <div>
            <CalendarWrapper title={title}>
                <DayPicker
                    locale="es"
                    initialMonth={months.jan}
                    selectedDays={clubDates}
                    navbarElement={<span></span>}
                    months={MONTHS}
                    weekdaysLong={WEEKDAYS}
                    weekdaysShort={WEEKDAYS_SHORT}
                    firstDayOfWeek={1}
                />
                <DayPicker
                    locale="es"
                    initialMonth={months.feb}
                    selectedDays={clubDates}
                    navbarElement={<span></span>}
                    months={MONTHS}
                    weekdaysLong={WEEKDAYS}
                    weekdaysShort={WEEKDAYS_SHORT}
                    firstDayOfWeek={1}
                />
                <DayPicker
                    locale="es"
                    initialMonth={months.mar}
                    selectedDays={clubDates}
                    navbarElement={<span></span>}
                    months={MONTHS}
                    weekdaysLong={WEEKDAYS}
                    weekdaysShort={WEEKDAYS_SHORT}
                    firstDayOfWeek={1}
                />
                <DayPicker
                    locale="es"
                    initialMonth={months.apr}
                    selectedDays={clubDates}
                    navbarElement={<span></span>}
                    months={MONTHS}
                    weekdaysLong={WEEKDAYS}
                    weekdaysShort={WEEKDAYS_SHORT}
                    firstDayOfWeek={1}
                />
                <DayPicker
                    locale="es"
                    initialMonth={months.may}
                    selectedDays={clubDates}
                    navbarElement={<span></span>}
                    months={MONTHS}
                    weekdaysLong={WEEKDAYS}
                    weekdaysShort={WEEKDAYS_SHORT}
                    firstDayOfWeek={1}
                />
                <DayPicker
                    locale="es"
                    initialMonth={months.jun}
                    selectedDays={clubDates}
                    navbarElement={<span></span>}
                    months={MONTHS}
                    weekdaysLong={WEEKDAYS}
                    weekdaysShort={WEEKDAYS_SHORT}
                    firstDayOfWeek={1}
                />
                <DayPicker
                    locale="es"
                    initialMonth={months.jul}
                    selectedDays={clubDates}
                    navbarElement={<span></span>}
                    months={MONTHS}
                    weekdaysLong={WEEKDAYS}
                    weekdaysShort={WEEKDAYS_SHORT}
                    firstDayOfWeek={1}
                    disabledDays={[
                        {
                            after: months.jun,
                            before: months.aug
                        }
                    ]}
                />
                <DayPicker
                    locale="es"
                    initialMonth={months.aug}
                    selectedDays={clubDates}
                    navbarElement={<span></span>}
                    months={MONTHS}
                    weekdaysLong={WEEKDAYS}
                    weekdaysShort={WEEKDAYS_SHORT}
                    firstDayOfWeek={1}
                    disabledDays={[
                        {
                            after: months.jul,
                            before: months.sep
                        }
                    ]}
                />
                <DayPicker
                    locale="es"
                    initialMonth={months.sep}
                    selectedDays={clubDates}
                    navbarElement={<span></span>}
                    months={MONTHS}
                    weekdaysLong={WEEKDAYS}
                    weekdaysShort={WEEKDAYS_SHORT}
                    firstDayOfWeek={1}
                />
                <DayPicker
                    locale="es"
                    initialMonth={months.oct}
                    selectedDays={clubDates}
                    navbarElement={<span></span>}
                    months={MONTHS}
                    weekdaysLong={WEEKDAYS}
                    weekdaysShort={WEEKDAYS_SHORT}
                    firstDayOfWeek={1}
                />
                <DayPicker
                    locale="es"
                    initialMonth={months.nov}
                    selectedDays={clubDates}
                    navbarElement={<span></span>}
                    months={MONTHS}
                    weekdaysLong={WEEKDAYS}
                    weekdaysShort={WEEKDAYS_SHORT}
                    firstDayOfWeek={1}
                />
                <DayPicker
                    locale="es"
                    initialMonth={months.dec}
                    selectedDays={clubDates}
                    navbarElement={<span></span>}
                    months={MONTHS}
                    weekdaysLong={WEEKDAYS}
                    weekdaysShort={WEEKDAYS_SHORT}
                    firstDayOfWeek={1}
                />
            </CalendarWrapper>
        </div>
    )
};

export default ClubCalendar;