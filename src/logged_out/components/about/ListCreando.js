import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Grid from '@material-ui/core/Grid';
import { creaciones } from '../../../utils/data'

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    maxWidth: 752,
  },
  demo: {
    backgroundColor: theme.palette.background.paper,
  },
  title: {
    margin: theme.spacing(4, 0, 2),
  },
}));

export default function ListCreando() {
  const classes = useStyles();
  return (
    <div className={classes.root}>

      <Grid container spacing={2} justify="center"  >

        <Grid item xs={12} md={12}>

          <div className={classes.demo}>
            <List dense={false}>
            {
                creaciones.map(element => (
                    <ListItem>
                        <ListItemIcon>
                        {element.icon}
                        </ListItemIcon>
                        <ListItemText>
                        {element.text}
                        </ListItemText>
                    </ListItem>
                ))
            }
            </List>
          </div>
        </Grid>
      </Grid>

    </div>
  );
}