import React from "react";
import PropTypes from "prop-types";
import { Grid, Typography, withWidth, IconButton, makeStyles } from "@material-ui/core";
import calculateSpacing from "../home/calculateSpacing";
import InstagramIcon from '@material-ui/icons/Instagram';
import FacebookIcon from '@material-ui/icons/Facebook';
import TwitterIcon from '@material-ui/icons/Twitter';

const useStyles = makeStyles((theme) => ({
  icon: {
    fontSize:"5em",
    [theme.breakpoints.up("xs")]: {
      fontSize: "2em"
    },
    [theme.breakpoints.up("sm")]: {
      fontSize: "5em"
    },

  }
}))

function AboutSec2(props) {
  const { width } = props;
  const classes = useStyles();

  const redes = [
    {
      color: "",
      icon: <FacebookIcon className={classes.icon} />,
      url: "https://www.facebook.com/Libreriamilibromx/"
    },
    {
      color: "",
      icon: <TwitterIcon  className={classes.icon} />,
      url: "https://twitter.com/milibromx"
    },
    {
      color: "",
      icon: <InstagramIcon className={classes.icon} />,
      url: "https://www.instagram.com/libreriamilibromx/"
    },
  ]

  return (
    <div style={{ backgroundColor: "#FFFFFF" }}>
      <div className="container-fluid lg-p-top">
        <Typography variant="h3" align="center" style={{marginBottom: "50px"}}>
          Historia
        </Typography>
        <div className="container-fluid">
          <Grid container spacing={calculateSpacing(width)}>
            <Grid item>
              <Typography variant="h5" component="p" >
                  El proyecto "mi libro mx" nació en el 2015, con la iniciativa de la pequeña Mariana, al hacer separadores de páginas para vender a sus amigos y posteriormente a clientes que fue conociendo. Al iniciar este viaje fue detectando sus necesidades como lector y necesidades de otros lectores que se acercaban a conocer su proyecto. 
              </Typography>
            </Grid>
            <Grid item>
            <Typography variant="h5" component="p" m={1}>
              Por lo que al día de hoy se hacen llegar  accesorios para libros y libros nuevos para todas las edades a domicilio desde Hermosillo, Sonora a todo México, además de iniciar  en el 2018 tres Clubs de Lectura en su ciudad: 
              </Typography>
            </Grid>
            <Grid item>
            <Typography variant="h5" component="p" m={1} >
                Si deseas conocer su proyecto emprendedor, síguela a través de sus redes sociales:
              </Typography>
            </Grid>
          </Grid>
          <Grid container direction="row" justify="center" >
            {redes.map(r => (
              <Grid item>
                <IconButton href={r.url} color="secondary">
                {r.icon}
                </IconButton>
              </Grid>
            ))}

          </Grid>
        </div>
      </div>
    </div>
  );
}

AboutSec2.propTypes = {
  width: PropTypes.string.isRequired
};

export default withWidth()(AboutSec2);
