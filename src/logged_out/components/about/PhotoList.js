import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import { photoList } from '../../../utils/data'

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    backgroundColor: theme.palette.background.paper,
  },
  gridList: {
    
    
    // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
    transform: 'translateZ(0)',
  },
  titleBar: {
    textAlign:"center",
    background: 'rgba(0,0,0,0.8)'
  },
  icon: {
    color: 'white',
  },
}));

export default function PhotoList() {
  
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <GridList cellHeight={'auto'} spacing={120} className={classes.gridList}>
        {photoList.map((tile) => (
          <GridListTile key={tile.img} cols={tile.featured ? 2 : 1} rows={tile.featured ? 2 : 1}>
            <img src={tile.img} width={"150px"}  alt={tile.title} />
            <GridListTileBar
              title={tile.title}
              subtitle={tile.subtitle}
              titlePosition="bottom"
              actionPosition="left"
              className={classes.titleBar}
            />
          </GridListTile>
        ))}
      </GridList>
    </div>
  );
}