import React from "react";
import PropTypes from "prop-types";
import { Grid, withWidth } from "@material-ui/core";
import calculateSpacing from "../home/calculateSpacing";
import PhotoList from './PhotoList';


function AboutSec3(props) {
  const { width } = props;
  return (
    <div style={{ backgroundColor: "#FFFFFF" }}>
      <div className="container-fluid lg-p-top">
        <div className="container-fluid">
          <Grid container direction="column" spacing={calculateSpacing(width)}>
              <Grid item sm={12}>
            <PhotoList/>
              </Grid>
          </Grid>
        </div>
      </div>
    </div>
  );
}

AboutSec3.propTypes = {
  width: PropTypes.string.isRequired
};

export default withWidth()(AboutSec3);
