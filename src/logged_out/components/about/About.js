import React, { Fragment } from "react";
import AboutSec1 from "./AboutSec1";
import AboutSec2 from "./AboutSec2";
import AboutSec3 from "./AboutSec3";

function About() {
  return (
    <Fragment>
      <AboutSec1/>
      <AboutSec2/>
      <AboutSec3/>
    </Fragment>
  );
}

export default About;