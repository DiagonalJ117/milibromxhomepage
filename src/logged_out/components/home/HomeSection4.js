import React from "react";
import PropTypes from "prop-types";
import { Grid, withWidth } from "@material-ui/core";
import calculateSpacing from "../home/calculateSpacing";
import PhotoGridRow from './PhotoGridRow';

function HomeSection4(props) {
  const { width } = props;
  return (
    <div style={{ backgroundColor: "#FFFFFF" }}>
      <div className="container-fluid lg-p-top">

        <div className="container-fluid">
          <Grid container direction="row" spacing={calculateSpacing(width)}>
              <Grid item sm={12}>

                  <PhotoGridRow/>
              </Grid>
          </Grid>

        </div>
      </div>
    </div>
  );
}

HomeSection4.propTypes = {
  width: PropTypes.string.isRequired
};

export default withWidth()(HomeSection4);
