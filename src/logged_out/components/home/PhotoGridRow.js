import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

import ImageMasonry from 'react-image-masonry'
// import images from '../assets/picturepool'

function importAll(r) {
  return r.keys().map(r);
}

const imglist = importAll(require.context('../../../assets/picturepool', false, /\.(png|jpe?g|svg)$/));


const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    backgroundColor: theme.palette.background.paper,
  },
  gridList: {
    
    direction: "row",
    // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
    transform: 'translateZ(0)',
  },
  titleBar: {
    textAlign:"center",
    background: 'rgba(0,0,0,0.8)'
  },
  icon: {
    color: 'white',
  },
}));






export default function PhotoGridRow() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      
      <ImageMasonry 
        imageUrls={imglist}
        numCols={5}
        animate={true}
        
      
      />
    </div>
  );
}