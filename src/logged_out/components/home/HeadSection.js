import React, { Fragment } from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import {
  Grid,
  Typography,
  Card,
  Button,
  Hidden,
  Box,
  withStyles,
  withWidth,
  isWidthUp,
  Paper
} from "@material-ui/core";
import headerImage from "../../../assets/envolturaimg.jpg";
import envolturas from '../../../assets/envolturas.jpg'
import separadores from '../../../assets/separadores.jpg'
import paqueteimg from '../../../assets/picturepool/pakt1.jpg'
import imaginariolibro from '../../../assets/picturepool/imaginariolibro.jpg'
import pdlibro from '../../../assets/picturepool/pdlibro.jpg'
import naranjamecanica from '../../../assets/picturepool/naranjaMecanica.jpg'
import orgulloyprejuiciolibro from '../../../assets/picturepool/orgulloyprejuiciolibro.jpg'
import envol1 from '../../../assets/picturepool/envol1.jpg'
import envol2 from '../../../assets/picturepool/envol2.jpg'
import Carousel from 'react-material-ui-carousel'

const styles = theme => ({
  extraLargeButtonLabel: {
    fontSize: theme.typography.body1.fontSize,
    [theme.breakpoints.up("sm")]: {
      fontSize: theme.typography.h6.fontSize
    }
  },
  extraLargeButton: {
    paddingTop: theme.spacing(1.5),
    paddingBottom: theme.spacing(1.5),
    [theme.breakpoints.up("xs")]: {
      paddingTop: theme.spacing(1),
      paddingBottom: theme.spacing(1)
    },
    [theme.breakpoints.up("lg")]: {
      paddingTop: theme.spacing(2),
      paddingBottom: theme.spacing(2)
    }
  },
  card: {
    boxShadow: theme.shadows[4],
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
    [theme.breakpoints.up("xs")]: {
      paddingTop: theme.spacing(3),
      paddingBottom: theme.spacing(3)
    },
    [theme.breakpoints.up("sm")]: {
      paddingTop: theme.spacing(5),
      paddingBottom: theme.spacing(5),
      paddingLeft: theme.spacing(4),
      paddingRight: theme.spacing(4)
    },
    [theme.breakpoints.up("md")]: {
      paddingTop: theme.spacing(5.5),
      paddingBottom: theme.spacing(5.5),
      paddingLeft: theme.spacing(5),
      paddingRight: theme.spacing(5)
    },
    [theme.breakpoints.up("lg")]: {
      paddingTop: theme.spacing(6),
      paddingBottom: theme.spacing(6),
      paddingLeft: theme.spacing(6),
      paddingRight: theme.spacing(6)
    },
    [theme.breakpoints.down("lg")]: {
      width: "auto"
    }
  },
  wrapper: {
    position: "relative",
    backgroundColor: theme.palette.secondary.main,
    paddingBottom: theme.spacing(2)
  },
  image: {
    maxWidth: "70%",
    
    verticalAlign: "center",
    borderRadius: theme.shape.borderRadius,
    boxShadow: theme.shadows[4]
  },
  container: {
    marginTop: theme.spacing(6),
    marginBottom: theme.spacing(12),
    [theme.breakpoints.down("md")]: {
      marginBottom: theme.spacing(9)
    },
    [theme.breakpoints.down("sm")]: {
      marginBottom: theme.spacing(6)
    },
    [theme.breakpoints.down("sm")]: {
      marginBottom: theme.spacing(3)
    }
  },
  containerFix: {
    [theme.breakpoints.up("md")]: {
      maxWidth: "none !important"
    }
  },
  waveBorder: {
    paddingTop: theme.spacing(4)
  }
});

function CarouselItem(props){
  
  return (
    <Paper>
      <Grid container justify="center">
      <Grid item>
        
          <img
            src={props.item.image}
            style={{maxWidth:"600px"}}
            height="500"
            alt=" "
        /> 
      </Grid>
      </Grid>
    </Paper>
)
}

function HeadSection(props) {
  const { classes, width } = props;
  var carouselitems = [
    {
        name: "Random Name #1",
        description: "Probably the most random thing you have ever seen!",
        image: envol2
    },
    {
        name: "Random Name #2",
        description: "Hello World!",
        image: orgulloyprejuiciolibro
    },
    {
      name: "Random Name #2",
      description: "Hello World!",
      image: paqueteimg
  },
  {
    name: "Random Name #2",
    description: "Hello World!",
    image: naranjamecanica
  },
  {
    name: "Random Name #2",
    description: "Hello World!",
    image: envol1
  },
  {
    name: "Random Name #2",
    description: "Hello World!",
    image: pdlibro
  },
  {
    name: "Random Name #2",
    description: "Hello World!",
    image: imaginariolibro
  },
  {
    name: "Random Name #2",
    description: "Hello World!",
    image: envolturas
  },
  {
    name: "Random Name #2",
    description: "Hello World!",
    image: separadores
  },
  {
    name: "Random Name #2",
    description: "Hello World!",
    image: headerImage
  },
  ]
  return (
    <Fragment>
      <div className={classNames("lg-p-top", classes.wrapper)}>
        <div className={classNames("container-fluid", classes.container)}>
          <Box display="flex" justifyContent="center" className="row">
            <Card
              className={classes.card}
              data-aos-delay="200"
              data-aos="zoom-in"
            >
              <div className={classNames(classes.containerFix, "container")}>
                <Box justifyContent="space-between" className="row">
                  <Grid item xs={12} md={5}>
                    <Box
                      display="flex"
                      flexDirection="column"
                      justifyContent="center"
                      height="100%"
                    >
                      <Box mb={4}>
                        <Typography
                          variant={isWidthUp("lg", width) ? "h3" : "h4"}
                        >
                          Librería a Domicilio
                        </Typography>
                      </Box>
                      <div>
                        <Box mb={2}>
                          <Typography
                            variant={isWidthUp("lg", width) ? "h6" : "body1"}
                            color="textSecondary"
                          >
                            Desde 2015 somos librería con servicio de envío a domicilio<span role="img" aria-label="car emoji">🚗</span>.
                            
                          </Typography>
                          <Typography
                            variant={isWidthUp("lg", width) ? "h6" : "body1"}
                            color="textSecondary"
                          >
                            Personalizamos cada entrega de libros, con envolturas para regalo con mucho amor<span role="img" aria-label="gift emoji">🎁</span>.
                            Utilizamos materiales que no lastiman a nuestro planeta<span role="img" aria-label="plant emoji">🌱</span>.
                            
                          </Typography>

                        </Box>
                        <Button
                          variant="contained"
                          color="secondary"
                          fullWidth
                          className={classes.extraLargeButton}
                          classes={{ label: classes.extraLargeButtonLabel }}
                          href="https://www.facebook.com/pg/Libreriamilibromx/shop/"
                        >
                          Consulta nuestro catálogo
                        </Button>
                      </div>
                    </Box>
                  </Grid>
                  <Hidden smDown >
                    <Grid item md={6} >
                    <Carousel timeout={200}>
                          {
                              carouselitems.map( (item, i) => <CarouselItem key={i} item={item} /> )
                          }
                      </Carousel>
                      {/* <img
                        src={headerImage}
                        className={classes.image}
                        alt="header image"
                      /> */}
                    </Grid>
                  </Hidden>
                </Box>
              </div>
            </Card>
          </Box>
        </div>
      </div>
      {/* <WaveBorder
        upperColor={theme.palette.secondary.main}
        lowerColor="#FFFFFF"
        className={classes.waveBorder}
        animationNegativeDelay={2}
      /> */}
    </Fragment>
  );
}

HeadSection.propTypes = {
  classes: PropTypes.object,
  width: PropTypes.string,
  theme: PropTypes.object
};

export default withWidth()(
  withStyles(styles, { withTheme: true })(HeadSection)
);
