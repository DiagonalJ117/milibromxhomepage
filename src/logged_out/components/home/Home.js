import React, { Fragment } from "react";
import PropTypes from "prop-types";
import HeadSection from "./HeadSection";
import FeatureSection from "./FeatureSection";
import HomeSection4 from "./HomeSection4";

function Home() {

  return (
    <Fragment>
      <HeadSection />
      <FeatureSection />
      <HomeSection4/>
      {/* <PricingSection /> */}
    </Fragment>
  );
}

Home.propTypes = {
  selectHome: PropTypes.func.isRequired
};

export default Home;
