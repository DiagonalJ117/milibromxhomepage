import React, { memo } from "react";
import PropTypes from "prop-types";
import { Switch } from "react-router-dom";
import PropsRoute from "../../shared/components/PropsRoute";
import Home from "./home/Home";
import Store from "./store/Store";
import About from "./about/About"
import Clubs from "./clubs/Clubs"


function Routing() {
  
  return (
    <Switch>

      <PropsRoute path="/clubs" component={Clubs} />
      <PropsRoute path="/store" component={Store} />
      <PropsRoute path="/about" component={About} />
      <PropsRoute path="/" component={Home} />
      
      )
    </Switch>
  );
}

Routing.propTypes = {
  blogposts: PropTypes.arrayOf(PropTypes.object),
  selectHome: PropTypes.func.isRequired,
  selectBlog: PropTypes.func.isRequired
};

export default memo(Routing);
